# Overview

The supplementary material is provided in three different folders:

* MTSA Tool & Translator
* Validation
* Complementary Specifications

## MTSA Tool & Translator

In this folder you can find:

* mtsa.jar: MTSA tool written in Java for discrete event controller synthesis. To correctly synthesise controllers from the .lts file specifications, the Java Virtual Machine can be started with a variable heap space. In the paper we used a heap size of 10G. In linux you can use the following command (please ensure you have at least 10G of RAM available):

```
java -Xmx10G -jar mtsa.jar
```

* MTSA-userGuide.pdf: Brief user guide for the MTSA tool. For synthesising a controller open the ".lts" file that contains the specification to the control problem, click on the "Parse" button, select "System" from the dropdown menu and finally click on the "Compose" button. Synthesised controllers may be viewed in the "Draw" tab. To model check the synthesised controller against any FLTL property included as an assert in the ".lts" file, go to "Check" -> "LTL property" and select the desired property to check.

* QLS_translator.py: Python tool for translating QLS Specifications into control problems that can be synthesised using MTSA. It's a simple parser that requires no additional libraries, so it should run in Python 2.7 and Python 3.* without any issues (tested in Python 2.7 and 3.5.3). To translate a file "specification.txt" included in the same directory as this tool, into an Iterator Control Problem (Definition 4.1) use the following command-line instruction. This will generate a ".lts" file in the same directory named "output.lts". To translate the QLS specification instead to a QLS Control Problem (Definition 3.1) replace "ITE" with "EXP".

```
python QLS_translator.py ./specification.txt ITE >> output.lts
```

## Complementary Specifications

In this folder you can find the MTSA and QLS specifications from which several controllers from the paper were built:

* running_example_explicit.lts: MTSA SGR(1) control problem for the mission presented in "Section 2: Discrete Event Control Problems."
* running_example_QLS.txt: QLS specification for the mission in "Section 3.1". Please refer to the description of the folder "Validation" to understand the keywords that are used and their meaning.
* running_example_QLS_EXP.lts: MTSA SGR(1) control problem that results of applying the translator "QLS_translator.py" to "running_example_QLS.txt" with "EXP" argument.
* running_example_QLS_ITE.lts: MTSA SGR(1) control problem with strong independent fairness that results of applying the translator "QLS_translator.py" to "running_example_QLS.txt" with "ITE" argument.

## Validation

This folder includes all the modelled missions in Section 5. The files that correspond to the QLS specification have an extension ".txt". The rest of the files correspond to the QLS Control Problem (Definition 3.1) and QLS via Iterator Control Problem (Definition 4.1), ending in "*_EXP.lts" and "*_ITE.lts", respectively.

We also include the file "Random_missions.txt" with the randomly selected missions from the catalogue. Each line corresponds to a different mission from the catalogue where values are separated by tabs: Year, Paper title, Description as it appears in the catalogue, catalogue classification tags.

To identify each QLS specification we use the first words of the paper's title. For instance, the QLS_specification "find_my_office.txt" corresponds to a mission from the paper "Find my office: Navigating real space from semantic descriptions". Note that some missions are similar enough that they are reported jointly. This is the case of "motion_planning_for_robotic.txt" which is the QLS specification that groups the types of missions described in the following papers (please refer to the file "Random_missions.txt" to see that all these missions are in essence "find path from source to goal avoiding collisions"):

* Motion planning for robotic manipulators with independent wrist joints.
* Robust belief space planning under intermittent sensing via a maximum eigenvalue-based bound.
* Motion planning with Satisfiability Modulo Theories.
* Planning under topological constraints using beam-graphs.
* RRTX: Asymptotically optimal single-query sampling-based motion planning with quick replanning.
* Time-based RRT algorithm for rendezvous planning of two dynamic systems.

These QLS text specifications must be interpreted as follows:

* "//" indicates a comment. At the beginning of each QLS specification we provide a brief explanation of the modelled mission using these comments.
* "set " is a reserved word from MTSA for defining sets of events. For this reason we use the reserved word "group " to identify the location sets from the QLS specification.
* LTS and fluents are defined as usual in MTSA.
* "ltl_property " is a reserved word from MTSA that we use as well in QLS to define the FLTL formulas (safety and liveness goals). Liveness assumptions (if any) are included in the set definition "assumption = {}" as a list of asserts, similar to the way they are included in MTSA. A safety FLTL property may be used to define a LTS for the Environment by using the prefix "constraint_" in its name (see mission "safe_navigation.txt").
