import sys

EXPLICIT = 0
ITERATOR = 1

def replace_dot_for_squarebrackets(txt):
    for i in range(len(txt)):
        if txt[i:i+3] == "go." or txt[i:i+3] == "at.":
            aux = ""
            begin = i+3
            j = begin
            while 1:
                aux += txt[j]
                try:
                    int(aux)
                except:
                    if j==begin:
                        #OTHER
                        pass
                    else:
                        #NUM
                        txt = txt[:begin-1] + "[" + aux[:-1].strip() + "]" + txt[begin+len(aux)-1:]
                    break
                    
                j += 1
        
        if txt[i:i+4] == "yes." or txt[i:i+3] == "no.":
            aux = ""
            if txt[i] == "y":
                begin = i+4
            else:
                begin = i+3
            j = begin
            while 1:
                aux += txt[j]
                try:
                    int(aux)
                except:
                    if j==begin:
                        pass
                    else:
                        #NUM
                        txt = txt[:begin-1] + "[" + aux[:-1].strip() + "]" + txt[begin+len(aux)-1:]
                    break
                    
                j += 1
    return txt

def add_set(dic,txt):
    values = txt.split("=")
    dic[values[0].strip()]=values[1].strip()

def add_prop(dic,txt):
    values = txt.split("=")
    dic[values[0].strip()]=values[1].strip()

def add_LTS(dic,txt):
    values = txt.split("=")
    dic[values[0].strip()]=txt

def add_fluent(dic,txt):
    values = txt.split("=")
    dic[values[0].strip()]=values[1].strip()

def add_assert(dic,txt):
    values = txt.split("=")
    dic[values[0].strip()]=values[1].strip()

def get_lines_from_file(filename):
    f = open(filename,'r')
    lines = f.readlines()
    f.close()
    return lines

def filter_lines(lines):
    new_lines = []
    for line in lines:
        if line.startswith("//"):
            continue
        if len(line.replace(" ","")) > 1:
            if line.endswith("\n"):
                line = line[:-1]
            new_lines.append(line)
    return new_lines

def get_alphabet(spec):
    alphabet = []
    words_prop = []
    words_lts = []
    
    for name in spec["properties"]:
        prop = spec["properties"][name]
        words = get_words(prop)
        words_prop.extend(words)

    #print("words prop",words_prop)

    for name in spec["LTS"]:
        lts = spec["LTS"][name]
        words = get_words(lts,is_lts=True)
        words_lts.extend(words)
        
    #print("words lts",words_lts)

    words_prop.extend(words_lts)
    words = words_prop

    
    for word in words:
        if word not in alphabet:
            if not (word.startswith("go.any") or word.startswith("at.any")):
                alphabet.append(word)

    #print("alphabet",alphabet)
    
    return alphabet

def get_words(txt,is_lts=False,only_events=False):
    txt = txt.replace("[",' ')
    txt = txt.replace("]",' ')
    txt = txt.replace("<",' ')
    txt = txt.replace(">",' ')
    txt = txt.replace("|",' ')
    txt = txt.replace("&",' ')
    txt = txt.replace("!",' ')
    txt = txt.replace(" W ",' ')
    txt = txt.replace(" X ",' ')
    txt = txt.replace(").",' ')
    txt = txt.replace("(",' ')
    txt = txt.replace(")",' ')
    txt = txt.replace("-",' ')
    txt = txt.replace(",",' ')
    txt = txt.replace("{",' ')
    txt = txt.replace("}",' ')
    txt = txt.replace("\n",' ')
    txt = txt.replace("\\",' ')
    txt = txt.replace("initially 0",' ')
    txt = txt.replace("initially 1",' ')
    txt = txt.replace("=",' ')

    values = txt.split(" ")
    words = []
    for val in values:
        if len(val) > 0:
            #Valid word
            if (val[0].isupper()):
                if only_events:
                    continue

                #Fluent or assert
                if val in spec["sets"]:
                    #print("fluent",val)
                    words.extend(get_words(spec["sets"][val]))
                elif val in spec["fluents"]:
                    #print("fluent",val)
                    words.extend(get_words(spec["fluents"][val]))
                elif val in spec["asserts"]:
                    #print("assert",val)
                    words.extend(get_words(spec["asserts"][val]))
                elif val in spec["LTS"]:
                    if not is_lts:
                        print("Error, an LTS should appear in a LTL property")
                    else:
                        #ignore, its an LTS name
                        pass
                else:
                    if not is_lts:
                        if not val == "Alphabet":
                            print("Check specification, couldn't classify:",val)
                    else:
                        #ignore, its an LTS name
                        pass
            else:
                words.append(val)

    return words
    

def classify_specification(lines):
    lines = filter_lines(lines)

    list_LTS = {}
    list_prop = {}
    list_sets = {}
    list_fluents = {}
    list_asserts = {}
    list_groups = {}
    aux_LTS = ""
    end_LTS = False
    assumption = ""
    for line in lines:
        if line.startswith("group "):
            values = line.split("=")
            numbers = values[1].strip()[1:-1].split(",")
            list_groups[values[0][6:].strip()] = numbers
        elif line.startswith("set "):
            add_set(list_sets,line[4:])
        elif line.startswith("ltl_property "):
            add_prop(list_prop,line[13:])
        elif line.startswith("fluent "):
            add_fluent(list_fluents,line[7:])
        elif line.startswith("assert "):
            add_assert(list_asserts,line[7:])
        elif line.startswith("assumption"):
            values = line.split("{")
            assumption = values[1][:-1]
        else:
            aux_LTS += line + "\n"
            if line.endswith("."):
                add_LTS(list_LTS,aux_LTS)
                aux_LTS = ""
    return {"groups":list_groups,"assumption":assumption,"asserts":list_asserts,"LTS":list_LTS,"properties":list_prop,"sets":list_sets,"fluents":list_fluents}

def find_explicit_locs(alphabet):
    explicit_locs = []
    for event in alphabet:
        if event.startswith("go.") or event.startswith("at."):
            values = event.split(".")
            try:
                int(values[1])
                if values[1] not in explicit_locs:
                    explicit_locs.append(values[1])
            except:
                pass

    return explicit_locs

def convert_to_fluent(txt):
    txt = txt.replace(".","_")
    txt = "F_" + txt
    return txt

def translate_spec(spec,target):
    new_spec = ""

    spec_mod = spec.copy()
    alphabet = get_alphabet(spec)

    explicit = find_explicit_locs(alphabet)

    go_prop = [""]*len(spec_mod["groups"])
    at_prop = [""]*len(spec_mod["groups"])

    i = 0
    for model in spec_mod["groups"]:
        if target == EXPLICIT:
            for elem in spec_mod["groups"][model]:
                go_prop[i] += "go."+elem+","
                at_prop[i] += "at."+elem+","                
            go_prop[i] = go_prop[i][:-1]
            at_prop[i] = at_prop[i][:-1]
        else:
            go_prop[i] += "go.same."+model+",go.next."+model
            at_prop[i] += "at."+model
        i += 1
    
    #Translating properties
    safety_list = []
    liveness_list = []
    for name in spec_mod["properties"]:
        prop = spec_mod["properties"][name]
        orig_values = prop.split("[")
        values = orig_values[1].split("<")
        if values[0].strip() == "]":
            #Liveness property
            begin = len(prop.split(">")[0])
            auxtxt = prop[begin+1:].strip()

            spec_mod["asserts"][name] = auxtxt
            liveness_list.append(name)
        else:
            #Safety property
            begin = len(prop.split("[")[0])
            auxtxt = prop[begin:]

            safety_list.append(name)

    i = 0
    for model in spec_mod["groups"]:
        end = "any."+model
        auxgotxt = "(" + go_prop[i].replace(","," || ") + ")"
        auxattxt = "(" + at_prop[i].replace(","," || ") + ")"
        for name in spec_mod["properties"]:
            spec_mod["properties"][name] = spec_mod["properties"][name].replace("go."+end,auxgotxt)
            spec_mod["properties"][name] = spec_mod["properties"][name].replace("at."+end,auxattxt)

        for name in spec_mod["asserts"]:
            spec_mod["asserts"][name] = spec_mod["asserts"][name].replace("go."+end,auxgotxt)
            spec_mod["asserts"][name] = spec_mod["asserts"][name].replace("at."+end,auxattxt)

        for name in spec_mod["fluents"]:
            spec_mod["fluents"][name] = spec_mod["fluents"][name].replace("go."+end,go_prop[i])
            spec_mod["fluents"][name] = spec_mod["fluents"][name].replace("at."+end,at_prop[i])

        for name in spec_mod["sets"]:
            spec_mod["sets"][name] = spec_mod["sets"][name].replace("go."+end,go_prop[i])
            spec_mod["sets"][name] = spec_mod["sets"][name].replace("at."+end,at_prop[i])

        for name in spec_mod["LTS"]:
            spec_mod["LTS"][name] = spec["LTS"][name].replace("go."+end,go_prop[i])
            spec_mod["LTS"][name] = spec["LTS"][name].replace("at."+end,at_prop[i])
        i += 1
    

    fluent_events = []
    for name in spec_mod["properties"]:
        words = get_words(spec_mod["properties"][name],only_events=True)
        replaced = []
        for word in words:
            if word not in replaced:
                spec_mod["properties"][name] = spec_mod["properties"][name].replace(word,convert_to_fluent(word))
                replaced.append(word)
            if word not in fluent_events:
                fluent_events.append(word)
    
    for name in spec_mod["asserts"]:
        words = get_words(spec_mod["asserts"][name],only_events=True)
        replaced = []
        for word in words:
            if word not in replaced:
                spec_mod["asserts"][name] = spec_mod["asserts"][name].replace(word,convert_to_fluent(word))
                replaced.append(word)
            if word not in fluent_events:
                fluent_events.append(word)

    fluent_txt = ""
    for event in fluent_events:
        event_fl = convert_to_fluent(event)
        fluent_txt += "fluent " + event_fl + " = <" + event + ",Alphabet\{" + event + "}>\n"
    if target == ITERATOR:
        for model in spec_mod["groups"]:
            fluent_txt += "fluent F_Many_"+model+" = <many."+model+",Alphabet\{many."+model+"}>\n"

    alphabet_txt = "set Alphabet = {"
    i = 0
    for model in spec_mod["groups"]:
        alphabet_txt += go_prop[i] + "," + at_prop[i] + ",visit."+model+","
        i+=1
    for i in range(len(explicit)):
        alphabet_txt += "go."+explicit[i]+",at."+explicit[i]+","
    alphabet_txt = alphabet_txt[:-1]
    for event in alphabet:
        if not (event.startswith("go.") or event.startswith("at.") or event.startswith("visit.")):
            alphabet_txt += "," + event
    alphabet_txt += "}\n"

    #Controllables
    controllables_txt = "set ControlActions = "
    if len(get_words(spec_mod["sets"]["Controllables"])) > 0:
        controllables_txt += spec_mod["sets"]["Controllables"][:-1] + ","
    else:
        controllables_txt += "{"
    i = 0
    for model in spec_mod["groups"]:
        controllables_txt += go_prop[i] + ","
        i+=1
    for i in range(len(explicit)):
        controllables_txt += "go."+explicit[i]+","
    controllables_txt = controllables_txt[:-1]
    controllables_txt += "}\n"

    #Movement model
    model_txt = ""
    for exp in explicit:
        model_txt += "Mov_EXP_"+exp+" = (go."+exp+" -> (at."+exp+" -> Mov_EXP_"+exp+")).\n"
    i = 0
    for model in spec_mod["groups"]:
        model_txt += "Mov_"+model+" = ({"+go_prop[i]+"} -> ({"+at_prop[i]+"} -> Mov_"+model+")).\n"
        if target == EXPLICIT:
            model_txt += "Mov_"+model+"_at = ("
            for elem in spec_mod["groups"][model]:
                model_txt += "\n\tgo."+elem+" -> (at."+elem+" -> Mov_"+model+"_at) |"
            model_txt = model_txt[:-2] + ").\n"
        i+=1
    model_txt += "Mod_turnos = ({"
    for exp in explicit:
        model_txt += "go."+exp+","
    for i in range(len(spec_mod["groups"])):
        model_txt += go_prop[i] + ","
    model_txt = model_txt[:-1] + "} -> ({"
    for exp in explicit:
        model_txt += "at."+exp+","
    for i in range(len(spec_mod["groups"])):
        model_txt += at_prop[i] + ","
    model_txt = model_txt[:-1] + "} -> Mod_turnos)).\n"

    if target == EXPLICIT:
        for model in spec_mod["groups"]:
            model_txt += "constraint Force_Visit_"+model+" = ([](X visit."+model+" -> ("
            for elem in spec_mod["groups"][model]:
                model_txt += "F_Visit_"+elem+" && "
            model_txt = model_txt[:-4] + ")) && !(visit."+model+"))\n"
            
            model_txt += "constraint Force_Visit2_"+model+" = [](("
            for elem in spec_mod["groups"][model]:
                model_txt += "F_Visit_"+elem+" && "
            model_txt = model_txt[:-4] + ") -> X(!{Alphabet\{visit."+model+"}} W visit."+model+"))\n"
        
    elif target == ITERATOR:
        i = 0
        for model in spec_mod["groups"]:
            model_txt += "ITE_"+model+ " = ({Alphabet\{"+go_prop[i]+",visit."+model+"}} -> ITE_"+model+" | go.next."+model+" -> GoBeforeOne_"+model+"),\n"
            model_txt += "GoBeforeOne_"+model+" = ({Alphabet\{at."+model+",visit."+model+"}} -> GoBeforeOne_"+model+" | at."+model+" -> AtBeforeOne_"+model+"),\n"
            model_txt += "AtBeforeOne_"+model+" = (many."+model+" -> ITE_"+model+" | one."+model+" -> AfterOne_"+model+"),\n"
            model_txt += "AfterOne_"+model+" = ({Alphabet\{"+go_prop[i]+",visit."+model+"}} -> AfterOne_"+model+" | go.same."+model+" -> GoSame_"+model+" | go.next."+model+" -> GoNext_"+model+"),\n"
            model_txt += "GoSame_"+model+" = ({Alphabet\{at."+model+",visit."+model+"}} -> GoSame_"+model+" | at."+model+" -> AfterOne_"+model+"),\n"
            model_txt += "GoNext_"+model+" = ({Alphabet\{at."+model+",visit."+model+"}} -> GoNext_"+model+" | at."+model+" -> AtNext_"+model+"),\n"
            model_txt += "AtNext_"+model+ " = (visit."+model+" -> ITE_"+model+").\n"
            i+=1

    #Environment
    environment_txt = "||Environment = (Mod_turnos"
    for exp in explicit:
        environment_txt += " || Mov_EXP_"+exp
    for model in spec_mod["groups"]:
        environment_txt += " || Mov_"+model
        
    if target == EXPLICIT:
        for model in spec_mod["groups"]:
            environment_txt += " || Mov_"+model+"_at"
            environment_txt += " || Force_Visit_"+model+" || Force_Visit2_"+model
    if target == ITERATOR:
        for model in spec_mod["groups"]:
            environment_txt += " || ITE_"+model
                
    for name in spec["LTS"]:
        environment_txt +=  " || " + name
    for name in safety_list:
        if name.startswith("constraint_"):
            environment_txt += " || " + name[11:]
    environment_txt += ").\n"


    #Compile the text
    new_spec += alphabet_txt
    new_spec += controllables_txt
    new_spec += "//User supplied SETS\n"
    for name in spec_mod["sets"]:
        if name == "Controllables":
            if len(get_words(spec_mod["sets"]["Controllables"])) == 0:
                continue
        new_spec += "set " + name + " = " + spec_mod["sets"][name] + "\n"

    new_spec += "\n"
    new_spec += "//Movement Model = "
    if target == ITERATOR:
        new_spec += "ITERATOR\n"
    elif target == EXPLICIT:
        new_spec += "EXPLICIT\n"
    new_spec += model_txt
    new_spec += "//User supplied LTS (translated)\n"
    for name in spec_mod["LTS"]:
        new_spec += spec_mod["LTS"][name]

    new_spec += "\n"
    new_spec += environment_txt

    new_spec += "\n"
    new_spec += "//Model dependent fluents:\n"
    new_spec += fluent_txt
    if target == EXPLICIT:
        for model in spec_mod["groups"]:
            for elem in spec_mod["groups"][model]:
                new_spec += "fluent F_Visit_"+elem+" = <at."+elem+",visit."+model+">\n"

    
    new_spec += "//User supplied FLUENTS (translated)\n"
    for name in spec_mod["fluents"]:
        new_spec += "fluent " + name + " = " + spec_mod["fluents"][name] + "\n"

    new_spec += "\n"
    
    
    new_spec += "//Model dependent ASSERTS (translated)\n"
    if target == ITERATOR:
        for model in spec_mod["groups"]:
            new_spec += "assert A_Many_"+model+" = (F_Many_"+model+")\n"
            
    
    
    new_spec += "\n"
    new_spec += "//User supplied ASSERTS (translated)\n"
    for name in spec_mod["asserts"]:
        if (name in liveness_list):
            continue
        new_spec += "assert " + name + " = " + spec_mod["asserts"][name] + "\n"

    new_spec += "\n"
    new_spec += "//User supplied SAFETY PROPERTIES (translated)\n"
    for name in safety_list:
        if name.startswith("constraint_"):
            new_spec += "constraint " + name[11:]+" = "+spec_mod["properties"][name] + "\n"
        else:
            new_spec += "ltl_property "+name+" = "+spec_mod["properties"][name] + "\n"

    new_spec += "\n"
    new_spec += "//User supplied LIVENESS PROPERTIES (translated)\n"
    for name in liveness_list:
        new_spec += "assert " + name + " = " + spec_mod["asserts"][name] + "\n"

    
    new_spec += "\n"
    new_spec += "//Controller\n"
    new_spec += "controller ||C = (Environment)~{Goal}.\n"

    new_spec += "\n"
    new_spec += "controllerSpec Goal = {\n"

    if target == ITERATOR:
        new_spec += "\t failure = {"
        for model in spec_mod["groups"]:
            new_spec += "A_Many_"+model+","
        new_spec += "}\n"
    new_spec += "\t safety = {"
    for name in safety_list:
        if not name.startswith("constraint_"):
            new_spec += name + ","
    new_spec += "}\n"
    new_spec += "\t assumption = {"
    if len(spec_mod["assumption"]) > 0:
        new_spec += spec_mod["assumption"] + ","
            
    new_spec += "}\n"
    new_spec += "\t liveness = {"
    for name in liveness_list:
        new_spec += name + ","
    new_spec += "}\n"
    new_spec += "\t controllable = {ControlActions}\n"
    new_spec += "}\n\n"
    new_spec += "||MinC = C.\n"
    new_spec += "||System = (MinC||Environment).\n"

    if target == ITERATOR:
        new_spec += "\n"
        new_spec += "//All these strong fairness properties must be checked to iterator-based controllers"
        for name in liveness_list:
            new_spec += "\n"
            new_spec += "assert Strong_Fairness_"+name+" = (("
            for model in spec_mod["groups"]:
                new_spec += "([]<>go.next."+model+" -> []<>one."+model+") && "
            if len(spec_mod["assumption"]) > 0:
                vals = spec_mod["assumption"].split(",")
                for val in vals:
                    new_spec += "[]<>"+val+" && "
            if len(spec_mod["groups"])>0 or len(spec_mod["assumption"]) > 0:
                new_spec = new_spec[:-4]
                if len(liveness_list) > 0:
                    new_spec += ") -> ("

            new_spec += "[]<>"+name
            new_spec += "))"

    new_spec = replace_dot_for_squarebrackets(new_spec)
            
    print("")
    print(new_spec)
    print("")


if (len(sys.argv) > 2):
    filename = sys.argv[1]
    if sys.argv[2] == "ITE":
        model_type = ITERATOR
    elif sys.argv[2] == "EXP":
        model_type = EXPLICIT
    else:
        print("Unrecognized model type")
        sys.exit()
else:
    print("Please specify a filename and model type")
    sys.exit()


lines = get_lines_from_file(filename)
spec = classify_specification(lines)
newspec = translate_spec(spec,model_type)





