//Can be modelled using quantifiers: 
//-- Locations in v3 and v18.
//Cannot be modelled using quantifiers:
//-- 

//We expanded into location sets v3 as set "a" and "v18" as set "b", and indicated that the goal of the mission was to visit all the locations from these regions once.

group a = {1,2}
group b = {10,11}

set Controllables = {}

fluent F_VisitedA = <visit.a,Alphabet\Alphabet>
fluent F_VisitedB = <visit.b,Alphabet\Alphabet>

ltl_property VisitedA = []<>(F_VisitedA)
ltl_property VisitedB = []<>(F_VisitedB)