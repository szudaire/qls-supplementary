set Alphabet = {go.same.car,go.next.car,at.car,visit.car,go[1],at[1],go[2],at[2],to.car,to.balancer,to.changer,yes.person,tire.avail,grab,no.person,drop}
set ControlActions = {grab,drop,go.same.car,go.next.car,go[1],go[2]}
//User supplied SETS
set Controllables = {grab,drop}
set AtAny = {at.car,at[1],at[2]}
set GoAny = {go.same.car,go.next.car,go[1],go[2]}

//Movement Model = ITERATOR
Mov_EXP_1 = (go[1]-> (at[1]-> Mov_EXP_1)).
Mov_EXP_2 = (go[2]-> (at[2]-> Mov_EXP_2)).
Mov_car = ({go.same.car,go.next.car} -> ({at.car} -> Mov_car)).
Mod_turnos = ({go[1],go[2],go.same.car,go.next.car} -> ({at[1],at[2],at.car} -> Mod_turnos)).
ITE_car = ({Alphabet\{go.same.car,go.next.car,visit.car}} -> ITE_car | go.next.car -> GoBeforeOne_car),
GoBeforeOne_car = ({Alphabet\{at.car,visit.car}} -> GoBeforeOne_car | at.car -> AtBeforeOne_car),
AtBeforeOne_car = (many.car -> ITE_car | one.car -> AfterOne_car),
AfterOne_car = ({Alphabet\{go.same.car,go.next.car,visit.car}} -> AfterOne_car | go.same.car -> GoSame_car | go.next.car -> GoNext_car),
GoSame_car = ({Alphabet\{at.car,visit.car}} -> GoSame_car | at.car -> AfterOne_car),
GoNext_car = ({Alphabet\{at.car,visit.car}} -> GoNext_car | at.car -> AtNext_car),
AtNext_car = (visit.car -> ITE_car).
//User supplied LTS (translated)
QueryPerson = ({GoAny} -> QueryPerson | {AtAny} -> AtQuestion),
AtQuestion = (no.person -> QueryPerson | yes.person -> YesQuestion),
YesQuestion = (tire.avail -> QueryPerson | {to.changer,to.balancer,to.car} -> QueryPerson).
Capabilities = (tire.avail -> Avail),
Avail = (grab -> Grabbed | tire.avail -> Avail),
Grabbed = (tire.avail -> Grabbed | drop -> Capabilities).
PersonMove = (to.changer -> (to.balancer -> (to.car -> PersonMove))).

||Environment = (Mod_turnos || Mov_EXP_1 || Mov_EXP_2 || Mov_car || ITE_car || QueryPerson || Capabilities || PersonMove || Balancer || Changer || Car).

//Model dependent fluents:
fluent F_tire_avail = <tire.avail,Alphabet\{tire.avail}>
fluent F_grab = <grab,Alphabet\{grab}>
fluent F_no_person = <no.person,Alphabet\{no.person}>
fluent F_yes_person = <yes.person,Alphabet\{yes.person}>
fluent F_go_same_car = <go.same.car,Alphabet\{go.same.car}>
fluent F_go_next_car = <go.next.car,Alphabet\{go.next.car}>
fluent F_drop = <drop,Alphabet\{drop}>
fluent F_go_2 = <go[2],Alphabet\{go[2]}>
fluent F_go_1 = <go[1],Alphabet\{go[1]}>
fluent F_visit_car = <visit.car,Alphabet\{visit.car}>
fluent F_Many_car = <many.car,Alphabet\{many.car}>
//User supplied FLUENTS (translated)
fluent RobotAtCar = <{at.car},GoAny>
fluent PersonAtChanger = <to.changer,{to.car,to.balancer}>
fluent PersonAtBalancer = <to.balancer,{to.car,to.changer}>
fluent RobotAtBalancer = <at[2],GoAny>
fluent RobotAtChanger = <at[1],GoAny>
fluent PersonAtCar = <to.car,{to.balancer,to.changer}> initially 1

//Model dependent ASSERTS (translated)
assert A_Many_car = (F_Many_car)

//User supplied ASSERTS (translated)
assert FoundPersonInChanger = (RobotAtChanger && PersonAtChanger && F_yes_person)
assert FoundPersonInBalancer = (RobotAtBalancer && PersonAtBalancer && F_yes_person)
assert FoundPersonInCarOrSearching = (RobotAtCar && PersonAtCar && (F_yes_person || F_visit_car))
assert A_GoAny = ((F_go_same_car || F_go_next_car) || F_go_1 || F_go_2)

//User supplied SAFETY PROPERTIES (translated)
ltl_property GrabTire = [](F_tire_avail -> (!A_GoAny W F_grab))
constraint Balancer = [](((RobotAtBalancer && PersonAtBalancer) -> !F_no_person) && ((RobotAtBalancer && !PersonAtBalancer) -> !F_yes_person))
ltl_property GoToChanger = []((RobotAtCar && F_tire_avail) -> (!(F_go_same_car || F_go_next_car) W (RobotAtChanger && F_drop)))
constraint Changer = [](((RobotAtChanger && PersonAtChanger) -> !F_no_person) && ((RobotAtChanger && !PersonAtChanger) -> !F_yes_person))
ltl_property GoToCar = []((RobotAtBalancer && F_tire_avail) -> (!F_go_2 W (RobotAtCar && F_drop)))
constraint Car = []((RobotAtCar && F_yes_person) -> PersonAtCar)
ltl_property GoToBalancer = []((RobotAtChanger && F_tire_avail) -> (!F_go_1 W (RobotAtBalancer && F_drop)))

//User supplied LIVENESS PROPERTIES (translated)
assert WithPerson = (FoundPersonInCarOrSearching || FoundPersonInChanger || FoundPersonInBalancer)

//Controller
controller ||C = (Environment)~{Goal}.

controllerSpec Goal = {
	 failure = {A_Many_car,}
	 safety = {GrabTire,GoToChanger,GoToCar,GoToBalancer,}
	 assumption = {}
	 liveness = {WithPerson,}
	 controllable = {ControlActions}
}

||MinC = C.
||System = (MinC||Environment).

//All these strong fairness properties must be checked to iterator-based controllers
assert Strong_Fairness_WithPerson = ((([]<>go.next.car -> []<>one.car)) -> ([]<>WithPerson))
