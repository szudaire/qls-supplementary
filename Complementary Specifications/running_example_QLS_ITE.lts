
set Alphabet = {go.same.u,go.next.u,at.u,visit.u,go.same.g,go.next.g,at.g,visit.g,gather,upload}
set ControlActions = {gather,upload,go.same.u,go.next.u,go.same.g,go.next.g}
//User supplied SETS
set Controllables = {gather,upload}
set GoAny = {go.same.g,go.next.g,go.same.u,go.next.u}

//Movement Model = ITERATOR
Mov_u = ({go.same.u,go.next.u} -> ({at.u} -> Mov_u)).
Mov_g = ({go.same.g,go.next.g} -> ({at.g} -> Mov_g)).
Mod_turnos = ({go.same.u,go.next.u,go.same.g,go.next.g} -> ({at.u,at.g} -> Mod_turnos)).
ITE_u = ({Alphabet\{go.same.u,go.next.u,visit.u}} -> ITE_u | go.next.u -> GoBeforeOne_u),
GoBeforeOne_u = ({Alphabet\{at.u,visit.u}} -> GoBeforeOne_u | at.u -> AtBeforeOne_u),
AtBeforeOne_u = (many.u -> ITE_u | one.u -> AfterOne_u),
AfterOne_u = ({Alphabet\{go.same.u,go.next.u,visit.u}} -> AfterOne_u | go.same.u -> GoSame_u | go.next.u -> GoNext_u),
GoSame_u = ({Alphabet\{at.u,visit.u}} -> GoSame_u | at.u -> AfterOne_u),
GoNext_u = ({Alphabet\{at.u,visit.u}} -> GoNext_u | at.u -> AtNext_u),
AtNext_u = (visit.u -> ITE_u).
ITE_g = ({Alphabet\{go.same.g,go.next.g,visit.g}} -> ITE_g | go.next.g -> GoBeforeOne_g),
GoBeforeOne_g = ({Alphabet\{at.g,visit.g}} -> GoBeforeOne_g | at.g -> AtBeforeOne_g),
AtBeforeOne_g = (many.g -> ITE_g | one.g -> AfterOne_g),
AfterOne_g = ({Alphabet\{go.same.g,go.next.g,visit.g}} -> AfterOne_g | go.same.g -> GoSame_g | go.next.g -> GoNext_g),
GoSame_g = ({Alphabet\{at.g,visit.g}} -> GoSame_g | at.g -> AfterOne_g),
GoNext_g = ({Alphabet\{at.g,visit.g}} -> GoNext_g | at.g -> AtNext_g),
AtNext_g = (visit.g -> ITE_g).
//User supplied LTS (translated)
Capabilities = ({GoAny} -> (gather -> Capabilities | upload -> Capabilities)).

||Environment = (Mod_turnos || Mov_u || Mov_g || ITE_u || ITE_g || Capabilities).

//Model dependent fluents:
fluent F_gather = <gather,Alphabet\{gather}>
fluent F_visit_g = <visit.g,Alphabet\{visit.g}>
fluent F_upload = <upload,Alphabet\{upload}>
fluent F_go_same_u = <go.same.u,Alphabet\{go.same.u}>
fluent F_go_next_u = <go.next.u,Alphabet\{go.next.u}>
fluent F_at_u = <at.u,Alphabet\{at.u}>
fluent F_go_same_g = <go.same.g,Alphabet\{go.same.g}>
fluent F_go_next_g = <go.next.g,Alphabet\{go.next.g}>
fluent F_Many_u = <many.u,Alphabet\{many.u}>
fluent F_Many_g = <many.g,Alphabet\{many.g}>
//User supplied FLUENTS (translated)
fluent WithData = <gather,upload>
fluent AtG = <{at.g},{GoAny}>
fluent AtU = <{at.u},{GoAny}>

//Model dependent ASSERTS (translated)
assert A_Many_u = (F_Many_u)
assert A_Many_g = (F_Many_g)

//User supplied ASSERTS (translated)
assert A_GoAny = ((F_go_same_g || F_go_next_g) || (F_go_same_u || F_go_next_u))

//User supplied SAFETY PROPERTIES (translated)
ltl_property GatherOnly = [](F_gather -> (AtG))
ltl_property UploadOnly = [](F_upload -> (AtU))
ltl_property UploadWithData = [](((F_at_u)) -> WithData)

//User supplied LIVENESS PROPERTIES (translated)
assert VisitG = (F_visit_g)
assert AtAnyU = ((F_go_same_u || F_go_next_u))

//Controller
controller ||C = (Environment)~{Goal}.

controllerSpec Goal = {
	 failure = {A_Many_u,A_Many_g,}
	 safety = {GatherOnly,UploadOnly,UploadWithData,}
	 assumption = {}
	 liveness = {VisitG,AtAnyU,}
	 controllable = {ControlActions}
}

||MinC = C.
||System = (MinC||Environment).

//All these strong fairness properties must be checked to iterator-based controllers
assert Strong_Fairness_VisitG = ((([]<>go.next.u -> []<>one.u) && ([]<>go.next.g -> []<>one.g)) -> ([]<>VisitG))
assert Strong_Fairness_AtAnyU = ((([]<>go.next.u -> []<>one.u) && ([]<>go.next.g -> []<>one.g)) -> ([]<>AtAnyU))

